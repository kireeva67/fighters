import { controls } from '../../constants/controls';

var keys = {
  'KeyA': true,
  'KeyD': true,
  'KeyJ': true,
  'KeyL': true,
};
var comboKeys = {
  'KeyQ': true,
  'KeyW': true,
  'KeyE': true,
  'KeyU': true,
  'KeyI': true,
  'KeyO': true,
}

var firstComboFlag = true;
var secondComboFlag = true;

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    bindKeys(firstFighter, secondFighter);
    bindComboKeys(firstFighter, secondFighter);
    let winner = isWinner(firstFighter, secondFighter);
    winner.then(resolve);
  });
}

export function getDamage(attacker, defender) {
  // return damage
  let attackerPower = getHitPower(attacker);
  let defenderPower = getBlockPower(defender);
  if(attackerPower > defenderPower){
    return attackerPower - defenderPower;
  }
  return 0;
}

export function getHitPower(fighter) {
  // return hit power
  let attack = fighter.attack;
  let criticalHitChance = randomChance(1,2);
  let hitPower = attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  // return block power
  let defense = fighter.defense;
  let dodgeChance = randomChance(1,2);
  let blockPower = defense * dodgeChance;
  return blockPower;
}

function randomChance(min, max){
  return Math.random() * (max - min) + min;
}

function bindKeys(firstFighter, secondFighter){
  updateHealthBar(firstFighter, secondFighter);  
  window.addEventListener('keydown', function(event) {
    if (event.code === 'KeyA') {
      keys['KeyA'] = false;
      if(keys['KeyD'] && keys['KeyL']){
        let damage = getDamage(firstFighter, secondFighter);
        secondFighter.health -= damage;
        updateHealthBar(firstFighter, secondFighter);
      }
    }
    if (event.code === 'KeyD') {
      keys['KeyD'] = false;
    }
    if (event.code === 'KeyJ') {
      keys['KeyJ'] = false;
      if(keys['KeyL'] && keys['KeyD']){
        let damage = getDamage(secondFighter, firstFighter);
        firstFighter.health -= damage;
        updateHealthBar(firstFighter, secondFighter);
      }
    }
    if (event.code === 'KeyL') {
      keys['KeyL'] = false;
    }
  });
  window.addEventListener('keyup', function(event) {
    if(event.code === 'KeyA') {
      keys['KeyA'] = true;
    }
    if(event.code === 'KeyD') {
      keys['KeyD'] = true;
    }
    if(event.code === 'KeyJ') {
      keys['KeyJ'] = true;
    }
    if(event.code === 'KeyL') {
      keys['KeyL'] = true;
    }
  });
}

async function isWinner(firstFighter, secondFighter){
  while(true){
    if (firstFighter.health <= 0){
      return secondFighter;
    }
    if (secondFighter.health <= 0){
      return firstFighter;
    }
    await sleep(100);
  }
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function updateHealthBar(firstFighter, secondFighter){
  if(firstFighter['maxHealth'] == undefined){
    firstFighter['maxHealth'] = firstFighter['health'];
  }
  if(secondFighter['maxHealth'] == undefined){
    secondFighter['maxHealth'] = secondFighter['health'];
  }
  let currentHealthFirst = (firstFighter['health'] * 100)/ firstFighter['maxHealth'];
  let currentHealthSecond = (secondFighter['health'] * 100)/ secondFighter['maxHealth'];
  let leftBar = document.getElementById('left-fighter-indicator');
  let rightBar = document.getElementById('right-fighter-indicator');
  leftBar.style.width = `${currentHealthFirst}%`;
  rightBar.style.width = `${currentHealthSecond}%`;
}

function bindComboKeys (firstFighter, secondFighter){
  window.addEventListener('keydown', function(event) {
    if(event.code === 'KeyQ'){
      comboKeys['KeyQ'] = false;
      if(comboKeys['KeyW'] === false && comboKeys['KeyE'] === false && firstComboFlag === true){
        secondFighter.health -= 2*(firstFighter.attack);
        updateHealthBar(firstFighter, secondFighter);
        firstComboFlag = false;
        setTimeout(allowComboToFirst, 10000);
      }
    }
    if(event.code === 'KeyW'){
      comboKeys['KeyW'] = false;
      if(comboKeys['KeyQ'] === false && comboKeys['KeyE'] === false && firstComboFlag === true){
        secondFighter.health -= 2*(firstFighter.attack);
        updateHealthBar(firstFighter, secondFighter);
        firstComboFlag = false;
        setTimeout(allowComboToFirst, 10000);
      }
    }
    if(event.code === 'KeyE'){
      comboKeys['KeyE'] = false;
      if(comboKeys['KeyW'] === false && comboKeys['KeyQ'] === false && firstComboFlag === true){
        secondFighter.health -= 2*(firstFighter.attack);
        updateHealthBar(firstFighter, secondFighter);
        firstComboFlag = false;
        setTimeout(allowComboToFirst, 10000);
      }
    }
    if(event.code === 'KeyU'){
      comboKeys['KeyU'] = false;
      if(comboKeys['KeyI'] === false && comboKeys['KeyO'] === false && secondComboFlag === true){
        firstFighter.health -= 2*(secondFighter.attack);
        updateHealthBar(firstFighter, secondFighter);
        secondComboFlag = false;
        setTimeout(allowComboToSecond, 10000);
      }
    }
    if(event.code === 'KeyI'){
      comboKeys['KeyI'] = false;
      if(comboKeys['KeyU'] === false && comboKeys['KeyO'] === false && secondComboFlag === true){
        firstFighter.health -= 2*(secondFighter.attack);
        updateHealthBar(firstFighter, secondFighter);
        secondComboFlag = false;
        setTimeout(allowComboToSecond, 10000);
      }
    }
    if(event.code === 'KeyO'){
      comboKeys['KeyO'] = false;
      if(comboKeys['KeyI'] === false && comboKeys['KeyU'] === false && secondComboFlag === true){
        firstFighter.health -= 2*(secondFighter.attack);
        updateHealthBar(firstFighter, secondFighter);
        secondComboFlag = false;
        setTimeout(allowComboToSecond, 10000);
      }
    }
  });
  window.addEventListener('keyup', function(event){
    if(event.code === 'KeyQ') {
      keys['KeyQ'] = true;
    }
    if(event.code === 'KeyW') {
      keys['KeyW'] = true;
    }
    if(event.code === 'KeyE') {
      keys['KeyE'] = true;
    }
    if(event.code === 'KeyU') {
      keys['KeyU'] = true;
    }
    if(event.code === 'KeyI') {
      keys['KeyI'] = true;
    }
    if(event.code === 'KeyO') {
      keys['KeyO'] = true;
    }
  });
}

function allowComboToFirst(){
  firstComboFlag = true;
}

function allowComboToSecond(){
  secondComboFlag = true;
}

