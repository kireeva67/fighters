import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if(fighter){
    const fighterName = createElement({
      tagName: 'div',
      className: 'fighter-preview__name',
    });
    fighterName.innerHTML = `${fighter.name}`;
    fighterElement.append(fighterName);

    const image = createFighterImage(fighter);
    fighterElement.append(image);

    let infoAttr = ['health', 'attack', 'defense'];
    for(let ch in infoAttr){
      const fighterInfo = createElement({
        tagName: 'div',
        className: 'fighter-preview__info',
      });
      fighterInfo.innerHTML = `${infoAttr[ch]} : ${fighter[infoAttr[ch]]}`;
      fighterElement.append(fighterInfo);
    }
  }  

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });
  return imgElement;
}
