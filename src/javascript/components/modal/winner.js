import { showModal } from "./modal";
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function
  let image = createElement({tagName: 'img', className: 'winnerImg', attributes: {'src': fighter.source}});
  showModal({title: `Winner: ${fighter.name}`, bodyElement: image, onClose: reload});
  
}

function reload(){
  location.reload();
  return false;
}
